# Albion Kills API

This REST API provides an interface to the events captured by [Albion Event Fetcher](https://gitlab.com/findley/albion-event-fetcher)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Updating Item Metadata
The `items.json` file is from https://github.com/ao-data/ao-bin-dumps/blob/master/formatted/items.json

## License
[MIT](https://choosealicense.com/licenses/mit/)
