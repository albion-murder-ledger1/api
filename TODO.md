- [ ] Organize API response models more better
- [ ] Implement date filter
- [ ] Update k8s to use kustomize like event-fetcher

- [x] Setup k8s deployment
- [x] Figure out how to get .env config inside (gitlab vars?) (k8s?)
- [x] Setup gitlab build
- [x] Dockerize
- [x] Load DB info from command line or config file
- [x] Build out API models
