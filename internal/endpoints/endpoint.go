package endpoints

import (
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/wI2L/fizz"
)

const (
	buildStatLimit      = 1000
	rating2v2Multiplier = 40.0
)

type endpoint interface {
	setup(ctx *EndpointCtx)
}

type EndpointCtx struct {
	DB                 *sqlx.DB
	GinEngine          *gin.Engine
	Fizz               *fizz.Fizz
	ItemNameMap        map[string]string
	TwitchClientId     string
	TwitchClientSecret string
}

var endpoints []endpoint = []endpoint{
	&getActiveStreams{},
	&getBuildStats{},
	&getEloDistributionEndpoint{},
	&getEloLeaderboardEndpoint{},
	&getHomePage{},
	&getItemImageEndpoint{},
	&getPlayer2v2BattlesEndpoint{},
	&getPlayer2v2Info{},
	&getPlayerBuildStats{},
	&getPlayerEloChart{},
	&getPlayerEventsEndpoint{},
	&getPlayerSearch{},
	&getPlayerWeaponMatrix{},
	&getPlayerWeaponStats{},
	&getPlayer{},
	&getRating2v2LeaderboardEndpoint{},
	&getSeasonsEndpoint{},
	&getTwitchUsersEndpoint{},
	&getVodEventsEndpoint{},
	&getWeaponMatrix{},
}

func Attach(ctx *EndpointCtx) {
	for _, ep := range endpoints {
		ep.setup(ctx)
	}
}
