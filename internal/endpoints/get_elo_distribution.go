package endpoints

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
	"time"
)

type getEloDistributionEndpoint struct {
	db                  *sqlx.DB
	distCacheSlayer     map[int]int
	distCacheStalker    map[int]int
	cacheExpiresSlayer  time.Time
	cacheExpiresStalker time.Time
}

type EloDistribution struct {
	Data map[int]int `json:"data"`
}

type GetEloDistributionParams struct {
	Type string `query:"type" default:"slayer" description:"CD Type (slayer, stalker)"`
}

func (ep *getEloDistributionEndpoint) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/elo-distribution", []fizz.OperationOption{
		fizz.Summary("Get 1v1 Elo Rating Distribution"),
		fizz.ID("get_elo_distribution"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getEloDistributionEndpoint) handle(c *gin.Context, params *GetEloDistributionParams) (*EloDistribution, error) {
	isSlayer := params.Type == "slayer"

	data, err := ep.fetchEloDistribution(isSlayer)

	if err != nil {
		return nil, errors.Trace(err)
	}

	return &EloDistribution{Data: data}, nil
}

func (ep *getEloDistributionEndpoint) fetchEloDistribution(isSlayer bool) (map[int]int, error) {
	var table string
	if isSlayer {
		table = "elo_slayer_1v1"
		if ep.distCacheSlayer != nil && time.Now().Before(ep.cacheExpiresSlayer) {
			return ep.distCacheSlayer, nil
		}
	} else {
		table = "elo_stalker_1v1"
		if ep.distCacheStalker != nil && time.Now().Before(ep.cacheExpiresStalker) {
			return ep.distCacheStalker, nil
		}
	}

	query := fmt.Sprintf(`select floor(rating/10)*10 as bin_floor, count(*)
 from %s
 group by bin_floor
 order by bin_floor;`, table)

	rows, err := ep.db.Query(query)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	cache := make(map[int]int)

	for rows.Next() {
		var bin int
		var count int

		err = rows.Scan(&bin, &count)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		cache[bin] = count
	}

	if isSlayer {
		ep.cacheExpiresSlayer = time.Now().Add(time.Hour * 24)
		ep.distCacheSlayer = cache
	} else {
		ep.cacheExpiresStalker = time.Now().Add(time.Hour * 24)
		ep.distCacheStalker = cache
	}

	return cache, nil
}
