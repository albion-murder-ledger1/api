package endpoints

import (
	"database/sql"
	"fmt"
	"sync"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"albion-kills-api/internal/models"
	"albion-kills-api/internal/syncstat"
)

type getHomePage struct {
	db             *sqlx.DB
	itemNameMap    map[string]string
	cacheTTL       time.Duration
	cachedHomePage *HomePageResponse
	isUpdating     bool
	lastError      error
	sync.Mutex
}

type HomePageResponse struct {
	JuicyKills     []*models.Event `json:"juicy_kills"`
	HighRankCDs    []*models.Event `json:"high_rank_cds"`
	StreamedFights []*models.Event `json:"streamed_fights"`
	LastUpdate     time.Time       `json:"last_update"`
}

func (ep *getHomePage) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB
	ep.itemNameMap = ctx.ItemNameMap
	ep.cacheTTL = time.Minute * 15
	ep.cachedHomePage = &HomePageResponse{}

	ctx.Fizz.GET("/api/home", []fizz.OperationOption{
		fizz.Summary("Dashdash board of interesting events"),
		fizz.ID("get_home"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getHomePage) handle(c *gin.Context) (*HomePageResponse, error) {
	r, err := ep.getHomePage()

	return r, err
}

func (ep *getHomePage) getHomePage() (*HomePageResponse, error) {
	if ep.needsUpdate() {
		go ep.updateHomePage()
	}

	return ep.cachedHomePage, ep.lastError
}

func (ep *getHomePage) updateHomePage() {
	ep.Lock()
	defer ep.Unlock()
	if ep.needsUpdate() {
		ep.isUpdating = true
		defer func() {
			ep.isUpdating = false
		}()
		juicyKills, err := ep.fetchJuicyKills()
		ep.lastError = err
		if err != nil {
			sentry.CaptureException(fmt.Errorf("Failed to fetch home page juicy kills: %w", err))
			return
		}

		highRankCDs, err := ep.fetchHighRankCDs()
		ep.lastError = err
		if err != nil {
			sentry.CaptureException(fmt.Errorf("Failed to fetch home page high rank CDs: %w", err))
			return
		}

		streamed, err := ep.fetchStreamed()
		ep.lastError = err
		if err != nil {
			sentry.CaptureException(fmt.Errorf("Failed to fetch home page streamed: %w", err))
			return
		}

		ep.cachedHomePage.JuicyKills = juicyKills
		ep.cachedHomePage.HighRankCDs = highRankCDs
		ep.cachedHomePage.StreamedFights = streamed
		ep.cachedHomePage.LastUpdate = time.Now()
	}
}

func (ep *getHomePage) needsUpdate() bool {
	return ep.cachedHomePage.LastUpdate.Before(time.Now().Add(-ep.cacheTTL)) && !ep.isUpdating
}

func (ep *getHomePage) fetchJuicyKills() ([]*models.Event, error) {
	eventsQuery := fmt.Sprintf(`
with
    top as (select event_id from events where time > DATE_SUB(NOW(), INTERVAL 3 DAY) order by total_kill_fame desc limit 5)
select %s, 0 as killer_rank, 0 as victim_rank, '' as vod
from events as e
join top as t on t.event_id = e.event_id
join killers as k on e.event_id = k.event_id and k.is_primary = 1
join victims as v on e.event_id = v.event_id
join loadouts as kl on k.loadout = kl.id
join loadouts as vl on v.loadout = vl.id
ORDER BY e.total_kill_fame DESC LIMIT 5`,
		basicEventProjection)

	rows, err := ep.db.Query(eventsQuery)

	if err != nil {
		return nil, fmt.Errorf("Failed to execute main query: %w\n", err)
	}

	defer rows.Close()

	events := make([]*models.Event, 0)

	for rows.Next() {
		event, err := ep.basicEventScan(rows)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		events = append(events, event)
	}

	return events, nil
}

func (ep *getHomePage) fetchHighRankCDs() ([]*models.Event, error) {
	lookbackHours := 24 + int(syncstat.Status.GetDelay(ep.db).Hours())

	eventsQuery := fmt.Sprintf(`
select %s, kelo.rank, velo.rank, "" as vod
from events as e
join killers as k on e.event_id = k.event_id and k.is_primary = 1
join victims as v on e.event_id = v.event_id
join elo_slayer_1v1 as kelo on kelo.name = k.name
join elo_slayer_1v1 as velo on velo.name = v.name
join loadouts as kl on k.loadout = kl.id
join loadouts as vl on v.loadout = vl.id
where e.time > date_sub(now(), interval %d hour)
  and kelo.rank != 0 and velo.rank != 0
  and participant_count = 1
  and party_size = 1
order by (kelo.rank + velo.rank) asc limit 5`,
		basicEventProjection, lookbackHours)

	rows, err := ep.db.Query(eventsQuery)

	if err != nil {
		return nil, fmt.Errorf("Failed to execute main query: %w\n", err)
	}

	defer rows.Close()

	events := make([]*models.Event, 0)

	for rows.Next() {
		event, err := ep.basicEventScan(rows)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		events = append(events, event)
	}

	return events, nil
}

func (ep *getHomePage) fetchStreamed() ([]*models.Event, error) {
	lookbackHours := 48 + int(syncstat.Status.GetDelay(ep.db).Hours())

	eventsQuery := fmt.Sprintf(`
select %s, 0 as killer_rank, 0 as victim_rank, vod.link
from twitch_vods as vod
join events as e on vod.event_id = e.event_id
join killers as k on e.event_id = k.event_id and k.is_primary = 1
join victims as v on e.event_id = v.event_id
join loadouts as kl on k.loadout = kl.id
join loadouts as vl on v.loadout = vl.id
where e.time > DATE_SUB(NOW(), INTERVAL %d hour)
ORDER BY vod.event_id DESC LIMIT 5`,
		basicEventProjection, lookbackHours)

	rows, err := ep.db.Query(eventsQuery)

	if err != nil {
		return nil, fmt.Errorf("Failed to execute main query: %w\n", err)
	}

	defer rows.Close()

	events := make([]*models.Event, 0)

	for rows.Next() {
		event, err := ep.basicEventScan(rows)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		events = append(events, event)
	}

	return events, nil
}

func (ep *getHomePage) basicEventScan(rows *sql.Rows) (*models.Event, error) {
	event := &models.Event{}

	err := rows.Scan(
		&event.Id, &event.Time, &event.TotalKillFame, &event.ParticipantCount, &event.PartySize, &event.Killer.Name,
		&event.Killer.IsPrimary, &event.Killer.KillFame, &event.Killer.DamageDone, &event.Killer.HealingDone, &event.Killer.ItemPower,
		&event.Killer.GuildName, &event.Killer.AllianceName, &event.Victim.Name, &event.Victim.ItemPower, &event.Victim.GuildName,
		&event.Victim.AllianceName, &event.Killer.Loadout.MainHand.Item, &event.Killer.Loadout.MainHand.Tier, &event.Killer.Loadout.MainHand.Enchant, &event.Killer.Loadout.MainHand.Quality,
		&event.Killer.Loadout.OffHand.Item, &event.Killer.Loadout.OffHand.Tier, &event.Killer.Loadout.OffHand.Enchant, &event.Killer.Loadout.OffHand.Quality, &event.Killer.Loadout.Head.Item,
		&event.Killer.Loadout.Head.Tier, &event.Killer.Loadout.Head.Enchant, &event.Killer.Loadout.Head.Quality, &event.Killer.Loadout.Body.Item, &event.Killer.Loadout.Body.Tier,
		&event.Killer.Loadout.Body.Enchant, &event.Killer.Loadout.Body.Quality, &event.Killer.Loadout.Shoe.Item, &event.Killer.Loadout.Shoe.Tier, &event.Killer.Loadout.Shoe.Enchant,
		&event.Killer.Loadout.Shoe.Quality, &event.Killer.Loadout.Bag.Item, &event.Killer.Loadout.Bag.Tier, &event.Killer.Loadout.Bag.Enchant, &event.Killer.Loadout.Bag.Quality,
		&event.Killer.Loadout.Cape.Item, &event.Killer.Loadout.Cape.Tier, &event.Killer.Loadout.Cape.Enchant, &event.Killer.Loadout.Cape.Quality, &event.Killer.Loadout.Mount.Item,
		&event.Killer.Loadout.Mount.Tier, &event.Killer.Loadout.Mount.Quality, &event.Killer.Loadout.Food.Item, &event.Killer.Loadout.Food.Tier, &event.Killer.Loadout.Food.Enchant,
		&event.Killer.Loadout.Potion.Item, &event.Killer.Loadout.Potion.Tier, &event.Killer.Loadout.Potion.Enchant, &event.Victim.Loadout.MainHand.Item, &event.Victim.Loadout.MainHand.Tier,
		&event.Victim.Loadout.MainHand.Enchant, &event.Victim.Loadout.MainHand.Quality, &event.Victim.Loadout.OffHand.Item, &event.Victim.Loadout.OffHand.Tier, &event.Victim.Loadout.OffHand.Enchant,
		&event.Victim.Loadout.OffHand.Quality, &event.Victim.Loadout.Head.Item, &event.Victim.Loadout.Head.Tier, &event.Victim.Loadout.Head.Enchant, &event.Victim.Loadout.Head.Quality,
		&event.Victim.Loadout.Body.Item, &event.Victim.Loadout.Body.Tier, &event.Victim.Loadout.Body.Enchant, &event.Victim.Loadout.Body.Quality, &event.Victim.Loadout.Shoe.Item,
		&event.Victim.Loadout.Shoe.Tier, &event.Victim.Loadout.Shoe.Enchant, &event.Victim.Loadout.Shoe.Quality, &event.Victim.Loadout.Bag.Item, &event.Victim.Loadout.Bag.Tier,
		&event.Victim.Loadout.Bag.Enchant, &event.Victim.Loadout.Bag.Quality, &event.Victim.Loadout.Cape.Item, &event.Victim.Loadout.Cape.Tier, &event.Victim.Loadout.Cape.Enchant,
		&event.Victim.Loadout.Cape.Quality, &event.Victim.Loadout.Mount.Item, &event.Victim.Loadout.Mount.Tier, &event.Victim.Loadout.Mount.Quality, &event.Victim.Loadout.Food.Item,
		&event.Victim.Loadout.Food.Tier, &event.Victim.Loadout.Food.Enchant, &event.Victim.Loadout.Potion.Item, &event.Victim.Loadout.Potion.Tier, &event.Victim.Loadout.Potion.Enchant,
		&event.Killer.Rank1v1, &event.Victim.Rank1v1, &event.Killer.Vod,
	)

	if err != nil {
		return nil, err
	}

	event.Killer.Loadout.ReconstructIds()
	event.Victim.Loadout.ReconstructIds()

	decorateLoadoutWithItemNames(ep.itemNameMap, &event.Killer.Loadout)
	decorateLoadoutWithItemNames(ep.itemNameMap, &event.Victim.Loadout)

	return event, nil
}

const basicEventProjection = `
e.event_id, UNIX_TIMESTAMP(e.time) as time, e.total_kill_fame, e.participant_count, e.party_size, k.name as k_name,
k.is_primary, k.kill_fame, k.damage_done, k.healing_done, k.item_power as k_item_power,
k.guild_name as k_guild_name, k.alliance_name as k_alliance_name, v.name as v_name, v.item_power as v_item_power, v.guild_name as v_guild_name,
v.alliance_name as v_alliance_name, kl.main_hand_item as k_main_hand_item, kl.main_hand_tier as k_main_hand_tier, kl.main_hand_enchant as k_main_hand_enchant, kl.main_hand_quality as k_main_hand_quality,
kl.off_hand_item as k_off_hand_item, kl.off_hand_tier as k_off_hand_tier, kl.off_hand_enchant as k_off_hand_enchant, kl.off_hand_quality as k_off_hand_quality, kl.head_item as k_head_item,
kl.head_tier as k_head_tier, kl.head_enchant as k_head_enchant, kl.head_quality as k_head_quality, kl.body_item as k_body_item, kl.body_tier as k_body_tier,
kl.body_enchant as k_body_enchant, kl.body_quality as k_body_quality, kl.shoe_item as k_shoe_item, kl.shoe_tier as k_shoe_tier, kl.shoe_enchant as k_shoe_enchant,
kl.shoe_quality as k_shoe_quality, kl.bag_item as k_bag_item, kl.bag_tier as k_bag_tier, kl.bag_enchant as k_bag_enchant, kl.bag_quality as k_bag_quality,
kl.cape_item as k_cape_item, kl.cape_tier as k_cape_tier, kl.cape_enchant as k_cape_enchant, kl.cape_quality as k_cape_quality, kl.mount_item as k_mount_item,
kl.mount_tier as k_mount_tier, kl.mount_quality as k_mount_quality, kl.food_item as k_food_item, kl.food_tier as k_food_tier, kl.food_enchant as k_food_enchant,
kl.potion_item as k_potion_item, kl.potion_tier as k_potion_tier, kl.potion_enchant as k_potion_enchant, vl.main_hand_item as v_main_hand_item, vl.main_hand_tier as v_main_hand_tier,
vl.main_hand_enchant as v_main_hand_enchant, vl.main_hand_quality as v_main_hand_quality, vl.off_hand_item as v_off_hand_item, vl.off_hand_tier as v_off_hand_tier, vl.off_hand_enchant as v_off_hand_enchant,
vl.off_hand_quality as v_off_hand_quality, vl.head_item as v_head_item, vl.head_tier as v_head_tier, vl.head_enchant as v_head_enchant, vl.head_quality as v_head_quality,
vl.body_item as v_body_item, vl.body_tier as v_body_tier, vl.body_enchant as v_body_enchant, vl.body_quality as v_body_quality, vl.shoe_item as v_shoe_item,
vl.shoe_tier as v_shoe_tier, vl.shoe_enchant as v_shoe_enchant, vl.shoe_quality as v_shoe_quality, vl.bag_item as v_bag_item, vl.bag_tier as v_bag_tier,
vl.bag_enchant as v_bag_enchant, vl.bag_quality as v_bag_quality, vl.cape_item as v_cape_item, vl.cape_tier as v_cape_tier, vl.cape_enchant as v_cape_enchant,
vl.cape_quality as v_cape_quality, vl.mount_item as v_mount_item, vl.mount_tier as v_mount_tier, vl.mount_quality as v_mount_quality, vl.food_item as v_food_item,
vl.food_tier as v_food_tier, vl.food_enchant as v_food_enchant, vl.potion_item as v_potion_item, vl.potion_tier as v_potion_tier, vl.potion_enchant as v_potion_enchant`
