package endpoints

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
)

type getPlayer struct {
	db *sqlx.DB
}

type GetPlayerParams struct {
	Name string `path:"name"`
}

type Player struct {
	Name           string `json:"name"`
	Elo1v1Slayer   int    `json:"elo_1v1_slayer"`
	Rank1v1Slayer  int    `json:"rank_1v1_slayer"`
	Elo1v1Stalker  int    `json:"elo_1v1_stalker"`
	Rank1v1Stalker int    `json:"rank_1v1_stalker"`
	Rating2v2      int    `json:"rating_2v2"`
	Rank2v2        int    `json:"rank_2v2"`
	FavWeapon      string `json:"fav_weapon"`
	FavWeapon1v1   string `json:"fav_weapon_1v1"`
	FavWeapon2v2   string `json:"fav_weapon_2v2"`
	TwitchName     string `json:"twitch_name,omitempty"`
}

func (ep *getPlayer) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/players/:name/info", []fizz.OperationOption{
		fizz.Summary("Get basic info for a player"),
		fizz.Response("404", "Player Not Found", nil, nil, nil),
		fizz.ID("get_player"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getPlayer) handle(c *gin.Context, params *GetPlayerParams) (*Player, error) {
	player, err := ep.fetchPlayer(params.Name)

	if err != nil {
		return nil, errors.Annotate(err, "Failed fetching player")
	}

	if player == nil {
		c.Status(http.StatusNotFound)
		return nil, errors.NotFoundf(`player "%s"`, params.Name)
	}

	return player, nil
}

func (ep *getPlayer) fetchPlayer(name string) (*Player, error) {
	query := `
select
    fav.name, fav.weapon, fav_1v1.weapon, fav_2v2.weapon, elo_slayer_1v1.rating, elo_slayer_1v1.rank,
    elo_stalker_1v1.rating, elo_stalker_1v1.rank, rating_2v2.rating, rating_2v2.rank, tw.twitch_name
from
player_fav_weapon as fav
left join player_fav_weapon_1v1 as fav_1v1 on fav.name = fav_1v1.name
left join player_fav_weapon_2v2 as fav_2v2 on fav.name = fav_2v2.name
left join elo_slayer_1v1 on elo_slayer_1v1.name = fav.name
left join elo_stalker_1v1 on elo_stalker_1v1.name = fav.name
left join rating_2v2 on rating_2v2.name = fav.name
left join twitch_users as tw on tw.name = fav.name
where fav.name = ?
`
	row := ep.db.QueryRow(query, name)

	var fav1v1 sql.NullString
	var fav2v2 sql.NullString
	var elo1v1Slayer sql.NullInt64
	var rank1v1Slayer sql.NullInt64
	var elo1v1Stalker sql.NullInt64
	var rank1v1Stalker sql.NullInt64
	var rating2v2 sql.NullFloat64
	var rank2v2 sql.NullInt64
	var twitchName sql.NullString

	player := &Player{}

	err := row.Scan(
		&player.Name,
		&player.FavWeapon,
		&fav1v1,
		&fav2v2,
		&elo1v1Slayer,
		&rank1v1Slayer,
		&elo1v1Stalker,
		&rank1v1Stalker,
		&rating2v2,
		&rank2v2,
		&twitchName,
	)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, fmt.Errorf("Failed fetch player: %v\n", err)
	}

	player.FavWeapon1v1 = fav1v1.String
	player.FavWeapon2v2 = fav2v2.String
	player.Elo1v1Slayer = int(elo1v1Slayer.Int64)
	player.Rank1v1Slayer = int(rank1v1Slayer.Int64)
	player.Elo1v1Stalker = int(elo1v1Stalker.Int64)
	player.Rank1v1Stalker = int(rank1v1Stalker.Int64)
	player.Rating2v2 = int(rating2v2.Float64 * rating2v2Multiplier)
	player.Rank2v2 = int(rank2v2.Int64)
	player.TwitchName = twitchName.String

	return player, nil
}
