package endpoints

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"albion-kills-api/internal/models"
	"albion-kills-api/internal/syncstat"
)

const loadoutProjection = `
{alias}.main_hand_item as {alias}_main_hand_item,
{alias}.main_hand_tier as {alias}_main_hand_tier,
{alias}.main_hand_enchant as {alias}_main_hand_enchant,
{alias}.main_hand_quality as {alias}_main_hand_quality,
{alias}.off_hand_item as {alias}_off_hand_item,
{alias}.off_hand_tier as {alias}_off_hand_tier,
{alias}.off_hand_enchant as {alias}_off_hand_enchant,
{alias}.off_hand_quality as {alias}_off_hand_quality,
{alias}.head_item as {alias}_head_item,
{alias}.head_tier as {alias}_head_tier,
{alias}.head_enchant as {alias}_head_enchant,
{alias}.head_quality as {alias}_head_quality,
{alias}.body_item as {alias}_body_item,
{alias}.body_tier as {alias}_body_tier,
{alias}.body_enchant as {alias}_body_enchant,
{alias}.body_quality as {alias}_body_quality,
{alias}.shoe_item as {alias}_shoe_item,
{alias}.shoe_tier as {alias}_shoe_tier,
{alias}.shoe_enchant as {alias}_shoe_enchant,
{alias}.shoe_quality as {alias}_shoe_quality,
{alias}.bag_item as {alias}_bag_item,
{alias}.bag_tier as {alias}_bag_tier,
{alias}.bag_enchant as {alias}_bag_enchant,
{alias}.bag_quality as {alias}_bag_quality,
{alias}.cape_item as {alias}_cape_item,
{alias}.cape_tier as {alias}_cape_tier,
{alias}.cape_enchant as {alias}_cape_enchant,
{alias}.cape_quality as {alias}_cape_quality,
{alias}.mount_item as {alias}_mount_item,
{alias}.mount_tier as {alias}_mount_tier,
{alias}.mount_quality as {alias}_mount_quality,
{alias}.food_item as {alias}_food_item,
{alias}.food_tier as {alias}_food_tier,
{alias}.food_enchant as {alias}_food_enchant,
{alias}.potion_item as {alias}_potion_item,
{alias}.potion_tier as {alias}_potion_tier,
{alias}.potion_enchant as {alias}_potion_enchant`

type getPlayer2v2BattlesEndpoint struct {
	db          *sqlx.DB
	itemNameMap map[string]string
}

type GetPlayer2v2BattlesParams struct {
	Name        string `path:"name"`
	Search      string `query:"q" default:"" description:"Search for player"`
	OwnWeapon   string `query:"own_weapon" default:"" description:"Filter events by this players weapon"`
	OtherWeapon string `query:"other_weapon" default:"" description:"Filter events by opponent weapon"`
	Skip        int    `query:"skip" default:"0" description:"Offset for paging data"`
	Take        int    `query:"take" default:"20" description:"Number of records to fetch" validate:"omitempty,max=50"`
}

func (ep *getPlayer2v2BattlesEndpoint) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB
	ep.itemNameMap = ctx.ItemNameMap

	ctx.Fizz.GET("/api/players/:name/battles/2v2", []fizz.OperationOption{
		fizz.Summary("Get 2v2 battles for player"),
		fizz.ID("get_player_2v2_battles"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getPlayer2v2BattlesEndpoint) handle(c *gin.Context, params *GetPlayerEventsParams) (*models.BattleResponse, error) {
	battles, err := ep.fetchPlayerBattles(
		params.Name,
		params.Take,
		params.Skip,
		params.Search,
		params.OwnWeapon,
		params.OtherWeapon,
	)

	if err != nil {
		return nil, errors.Annotate(err, "Failed fetching player 2v2 battles")
	}

	r := &models.BattleResponse{
		Battles:   battles,
		Skip:      params.Skip,
		Take:      params.Take,
		SyncDelay: int(syncstat.Status.GetDelay(ep.db).Seconds()),
	}

	return r, nil
}

func (ep *getPlayer2v2BattlesEndpoint) fetchPlayerBattles(
	name string,
	take,
	skip int,
	q,
	ownWeapon,
	otherWeapon string,
) ([]models.Battle, error) {

	projection := fmt.Sprintf(`
        b.battle_id, UNIX_TIMESTAMP(b.time) as time, winner_1, winner_2, loser_1, loser_2,
        %s, %s, %s, %s`,
		strings.ReplaceAll(loadoutProjection, "{alias}", "wl1"),
		strings.ReplaceAll(loadoutProjection, "{alias}", "wl2"),
		strings.ReplaceAll(loadoutProjection, "{alias}", "ll1"),
		strings.ReplaceAll(loadoutProjection, "{alias}", "ll2"),
	)

	// searchPred := "(1=1)"
	// if q != "" {
	// 	searchPred = `
	// (
	// v.name LIKE :q OR
	// v.guild_name LIKE :q OR
	// v.alliance_name LIKE :q OR
	// k.name LIKE :q OR
	// k.guild_name LIKE :q OR
	// k.alliance_name LIKE :q
	// )`
	// }
	// searchTerm := fmt.Sprintf("%%%s%%", q)

	// ownWeaponPredKiller := "(1=1)"
	// ownWeaponPredVictim := "(1=1)"
	// if ownWeapon != "" {
	// 	ownWeaponPredKiller = `(kl.main_hand_item = :ownWeapon)`
	// 	ownWeaponPredVictim = `(vl.main_hand_item = :ownWeapon)`
	// }

	// otherWeaponPredKiller := "(1=1)"
	// otherWeaponPredVictim := "(1=1)"
	// if otherWeapon != "" {
	// 	otherWeaponPredKiller = `(vl.main_hand_item = :otherWeapon)`
	// 	otherWeaponPredVictim = `(kl.main_hand_item = :otherWeapon)`
	// }

	battleQuery := fmt.Sprintf(`
select %s from 2v2_hg_battle as b
join loadouts as wl1 on b.winner_1_loadout = wl1.id
join loadouts as wl2 on b.winner_2_loadout = wl2.id
join loadouts as ll1 on b.loser_1_loadout = ll1.id
join loadouts as ll2 on b.loser_2_loadout = ll2.id
where (winner_1 = :name OR winner_2 = :name OR loser_1 = :name OR loser_2 = :name)
order by time desc LIMIT :limit OFFSET :offset;
    `,
		projection,
	)

	rows, err := ep.db.NamedQuery(battleQuery, map[string]interface{}{
		"name":   name,
		"limit":  take,
		"offset": skip,
		// "q":           searchTerm,
		// "ownWeapon":   ownWeapon,
		// "otherWeapon": otherWeapon,
	})

	if err != nil {
		return nil, fmt.Errorf("Failed to execute main query: %v\n", err)
	}

	defer rows.Close()

	battles := make([]models.Battle, 0)

	for rows.Next() {
		battle := models.Battle{}
		winner1 := models.BattlePlayer{}
		winner2 := models.BattlePlayer{}
		loser1 := models.BattlePlayer{}
		loser2 := models.BattlePlayer{}

		err = rows.Scan(
			&battle.Id, &battle.Time, &winner1.Name, &winner2.Name, &loser1.Name, &loser2.Name,

			&winner1.Loadout.MainHand.Item, &winner1.Loadout.MainHand.Tier, &winner1.Loadout.MainHand.Enchant, &winner1.Loadout.MainHand.Quality,
			&winner1.Loadout.OffHand.Item, &winner1.Loadout.OffHand.Tier, &winner1.Loadout.OffHand.Enchant, &winner1.Loadout.OffHand.Quality,
			&winner1.Loadout.Head.Item, &winner1.Loadout.Head.Tier, &winner1.Loadout.Head.Enchant, &winner1.Loadout.Head.Quality,
			&winner1.Loadout.Body.Item, &winner1.Loadout.Body.Tier, &winner1.Loadout.Body.Enchant, &winner1.Loadout.Body.Quality,
			&winner1.Loadout.Shoe.Item, &winner1.Loadout.Shoe.Tier, &winner1.Loadout.Shoe.Enchant, &winner1.Loadout.Shoe.Quality,
			&winner1.Loadout.Bag.Item, &winner1.Loadout.Bag.Tier, &winner1.Loadout.Bag.Enchant, &winner1.Loadout.Bag.Quality,
			&winner1.Loadout.Cape.Item, &winner1.Loadout.Cape.Tier, &winner1.Loadout.Cape.Enchant, &winner1.Loadout.Cape.Quality,
			&winner1.Loadout.Mount.Item, &winner1.Loadout.Mount.Tier, &winner1.Loadout.Mount.Quality,
			&winner1.Loadout.Food.Item, &winner1.Loadout.Food.Tier, &winner1.Loadout.Food.Enchant,
			&winner1.Loadout.Potion.Item, &winner1.Loadout.Potion.Tier, &winner1.Loadout.Potion.Enchant,

			&winner2.Loadout.MainHand.Item, &winner2.Loadout.MainHand.Tier, &winner2.Loadout.MainHand.Enchant, &winner2.Loadout.MainHand.Quality,
			&winner2.Loadout.OffHand.Item, &winner2.Loadout.OffHand.Tier, &winner2.Loadout.OffHand.Enchant, &winner2.Loadout.OffHand.Quality,
			&winner2.Loadout.Head.Item, &winner2.Loadout.Head.Tier, &winner2.Loadout.Head.Enchant, &winner2.Loadout.Head.Quality,
			&winner2.Loadout.Body.Item, &winner2.Loadout.Body.Tier, &winner2.Loadout.Body.Enchant, &winner2.Loadout.Body.Quality,
			&winner2.Loadout.Shoe.Item, &winner2.Loadout.Shoe.Tier, &winner2.Loadout.Shoe.Enchant, &winner2.Loadout.Shoe.Quality,
			&winner2.Loadout.Bag.Item, &winner2.Loadout.Bag.Tier, &winner2.Loadout.Bag.Enchant, &winner2.Loadout.Bag.Quality,
			&winner2.Loadout.Cape.Item, &winner2.Loadout.Cape.Tier, &winner2.Loadout.Cape.Enchant, &winner2.Loadout.Cape.Quality,
			&winner2.Loadout.Mount.Item, &winner2.Loadout.Mount.Tier, &winner2.Loadout.Mount.Quality,
			&winner2.Loadout.Food.Item, &winner2.Loadout.Food.Tier, &winner2.Loadout.Food.Enchant,
			&winner2.Loadout.Potion.Item, &winner2.Loadout.Potion.Tier, &winner2.Loadout.Potion.Enchant,

			&loser1.Loadout.MainHand.Item, &loser1.Loadout.MainHand.Tier, &loser1.Loadout.MainHand.Enchant, &loser1.Loadout.MainHand.Quality,
			&loser1.Loadout.OffHand.Item, &loser1.Loadout.OffHand.Tier, &loser1.Loadout.OffHand.Enchant, &loser1.Loadout.OffHand.Quality,
			&loser1.Loadout.Head.Item, &loser1.Loadout.Head.Tier, &loser1.Loadout.Head.Enchant, &loser1.Loadout.Head.Quality,
			&loser1.Loadout.Body.Item, &loser1.Loadout.Body.Tier, &loser1.Loadout.Body.Enchant, &loser1.Loadout.Body.Quality,
			&loser1.Loadout.Shoe.Item, &loser1.Loadout.Shoe.Tier, &loser1.Loadout.Shoe.Enchant, &loser1.Loadout.Shoe.Quality,
			&loser1.Loadout.Bag.Item, &loser1.Loadout.Bag.Tier, &loser1.Loadout.Bag.Enchant, &loser1.Loadout.Bag.Quality,
			&loser1.Loadout.Cape.Item, &loser1.Loadout.Cape.Tier, &loser1.Loadout.Cape.Enchant, &loser1.Loadout.Cape.Quality,
			&loser1.Loadout.Mount.Item, &loser1.Loadout.Mount.Tier, &loser1.Loadout.Mount.Quality,
			&loser1.Loadout.Food.Item, &loser1.Loadout.Food.Tier, &loser1.Loadout.Food.Enchant,
			&loser1.Loadout.Potion.Item, &loser1.Loadout.Potion.Tier, &loser1.Loadout.Potion.Enchant,

			&loser2.Loadout.MainHand.Item, &loser2.Loadout.MainHand.Tier, &loser2.Loadout.MainHand.Enchant, &loser2.Loadout.MainHand.Quality,
			&loser2.Loadout.OffHand.Item, &loser2.Loadout.OffHand.Tier, &loser2.Loadout.OffHand.Enchant, &loser2.Loadout.OffHand.Quality,
			&loser2.Loadout.Head.Item, &loser2.Loadout.Head.Tier, &loser2.Loadout.Head.Enchant, &loser2.Loadout.Head.Quality,
			&loser2.Loadout.Body.Item, &loser2.Loadout.Body.Tier, &loser2.Loadout.Body.Enchant, &loser2.Loadout.Body.Quality,
			&loser2.Loadout.Shoe.Item, &loser2.Loadout.Shoe.Tier, &loser2.Loadout.Shoe.Enchant, &loser2.Loadout.Shoe.Quality,
			&loser2.Loadout.Bag.Item, &loser2.Loadout.Bag.Tier, &loser2.Loadout.Bag.Enchant, &loser2.Loadout.Bag.Quality,
			&loser2.Loadout.Cape.Item, &loser2.Loadout.Cape.Tier, &loser2.Loadout.Cape.Enchant, &loser2.Loadout.Cape.Quality,
			&loser2.Loadout.Mount.Item, &loser2.Loadout.Mount.Tier, &loser2.Loadout.Mount.Quality,
			&loser2.Loadout.Food.Item, &loser2.Loadout.Food.Tier, &loser2.Loadout.Food.Enchant,
			&loser2.Loadout.Potion.Item, &loser2.Loadout.Potion.Tier, &loser2.Loadout.Potion.Enchant,
		)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		winner1.Loadout.ReconstructIds()
		winner2.Loadout.ReconstructIds()
		loser1.Loadout.ReconstructIds()
		loser2.Loadout.ReconstructIds()

		decorateLoadoutWithItemNames(ep.itemNameMap, &winner1.Loadout)
		decorateLoadoutWithItemNames(ep.itemNameMap, &winner2.Loadout)
		decorateLoadoutWithItemNames(ep.itemNameMap, &loser1.Loadout)
		decorateLoadoutWithItemNames(ep.itemNameMap, &loser2.Loadout)

		battle.Winners = []models.BattlePlayer{winner1, winner2}
		battle.Losers = []models.BattlePlayer{loser1, loser2}

		battles = append(battles, battle)
	}

	return battles, nil
}
