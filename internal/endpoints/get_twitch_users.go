package endpoints

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"net/http"

	"albion-kills-api/internal/models"
)

type getTwitchUsersEndpoint struct {
	db *sqlx.DB
}

func (ep *getTwitchUsersEndpoint) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.GinEngine.GET("/api/twitch-users", ep.handle)
}

func (ep *getTwitchUsersEndpoint) handle(c *gin.Context) {
	users, err := fetchTwitchUsers(ep.db)

	if err != nil {
		c.Error(fmt.Errorf("Failed fetching twitch users: %v\n", err))
		c.Status(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, users)
}

func fetchTwitchUsers(db *sqlx.DB) ([]*models.TwitchUser, error) {
	query := `select name, twitch_name, twitch_user_id from twitch_users where twitch_user_id != 0`

	rows, err := db.Query(query)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]*models.TwitchUser, 0)

	for rows.Next() {
		user := &models.TwitchUser{}

		err = rows.Scan(&user.Name, &user.TwitchUsername, &user.TwitchUserId)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		users = append(users, user)
	}

	return users, nil
}
