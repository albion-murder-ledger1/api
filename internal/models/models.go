package models

import (
	"database/sql"
	"strconv"
	"time"
)

type TwitchUser struct {
	Name           string `json:"name"`
	TwitchUsername string `json:"twitch_username"`
	TwitchUserId   int    `json:"-"`
}

type TwitchToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
	TokenType   string `json:"token_type"`
}

type TwitchStreamsResponse struct {
	Data       []TwitchStream   `json:"data"`
	Pagination TwitchPagination `json:"pagination"`
}
type TwitchStream struct {
	ID           string    `json:"id"`
	UserID       string    `json:"user_id"`
	UserLogin    string    `json:"user_login"`
	UserName     string    `json:"user_name"`
	GameID       string    `json:"game_id"`
	GameName     string    `json:"game_name"`
	Type         string    `json:"type"`
	Title        string    `json:"title"`
	ViewerCount  int       `json:"viewer_count"`
	StartedAt    time.Time `json:"started_at"`
	Language     string    `json:"language"`
	ThumbnailURL string    `json:"thumbnail_url"`
	TagIds       []string  `json:"tag_ids"`
	IsMature     bool      `json:"is_mature"`
}
type TwitchPagination struct {
	Cursor string `json:"cursor"`
}

type LeaderboardUser struct {
	Name               string `json:"name"`
	Rank               int    `json:"rank"`
	Rating             int    `json:"rating"`
	LastUpdate         int64  `json:"last_update,omitempty"`
	FavoriteWeaponItem string `json:"favorite_weapon_item"`
	FavoriteWeaponName string `json:"favorite_weapon_name"`
	TwitchUsername     string `json:"twitch_username,omitempty"`
}

type Season struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	StartDate int    `json:"start_date"`
	EndDate   int    `json:"end_date"`
	IsCurrent bool   `json:"is_current"`
}

type EventsResponse struct {
	Events     []Event `json:"events"`
	TotalCount int     `json:"total_count,omitempty"`
	Skip       int     `json:"skip"`
	Take       int     `json:"take"`
	SyncDelay  int     `json:"sync_delay"`
}

type LeaderboardResponse struct {
	Data       []*LeaderboardUser `json:"data"`
	TotalCount int                `json:"total_count,omitempty"`
	Skip       int                `json:"skip"`
	Take       int                `json:"take"`
	SyncDelay  int                `json:"sync_delay"`
}

type Event struct {
	Time             int    `json:"time"`
	Id               int    `json:"id"`
	BattleId         int    `json:"battle_id"`
	Killer           Killer `json:"killer"`
	Victim           Player `json:"victim"`
	TotalKillFame    int    `json:"total_kill_fame"`
	ParticipantCount int    `json:"participant_count"`
	PartySize        int    `json:"party_size"`
	Tags             Tags   `json:"tags"`
}

type Tags struct {
	Is1v1  bool `json:"is_1v1"`
	Is2v2  bool `json:"is_2v2"`
	Is5v5  bool `json:"is_5v5"`
	IsZvZ  bool `json:"is_zvz"`
	Fair   bool `json:"fair"`
	Unfair bool `json:"unfair"`
}

type Player struct {
	Name         string         `json:"name"`
	ItemPower    int            `json:"item_power"`
	GuildName    string         `json:"guild_name"`
	AllianceName string         `json:"alliance_name"`
	Loadout      Loadout        `json:"loadout"`
	Vod          string         `json:"vod"`
	DbVod        sql.NullString `json:"-"`
	EloChange    *int64         `json:"elo_change,omitempty"`
	Rank1v1      int            `json:"rank_1v1,omitempty"`
}

type Killer struct {
	Player
	IsPrimary   bool `json:"is_primary"`
	KillFame    int  `json:"kill_fame"`
	DamageDone  int  `json:"damage_done"`
	HealingDone int  `json:"healing_done"`
}

type Loadout struct {
	MainHand Item `json:"main_hand"`
	OffHand  Item `json:"off_hand"`
	Head     Item `json:"head"`
	Body     Item `json:"body"`
	Shoe     Item `json:"shoe"`
	Bag      Item `json:"bag"`
	Cape     Item `json:"cape"`
	Mount    Item `json:"mount"`
	Food     Item `json:"food"`
	Potion   Item `json:"potion"`
}

type Item struct {
	Id      string `json:"id"`
	Item    string `json:"type"`
	Tier    int    `json:"tier"`
	Enchant int    `json:"enchant"`
	Quality int    `json:"quality"`
	Name    string `json:"en_name"`
}

func (l *Loadout) ReconstructIds() {
	l.MainHand.reconstructId()
	l.OffHand.reconstructId()
	l.Head.reconstructId()
	l.Body.reconstructId()
	l.Shoe.reconstructId()
	l.Bag.reconstructId()
	l.Cape.reconstructId()
	l.Mount.reconstructId()
	l.Food.reconstructId()
	l.Potion.reconstructId()
}

func (i *Item) reconstructId() {
	tierString := ""
	if i.Tier > 0 {
		tierString = "T" + strconv.Itoa(i.Tier) + "_"
	}

	enchantString := ""
	if i.Enchant > 0 {
		enchantString = "@" + strconv.Itoa(i.Enchant)
	}

	i.Id = tierString + i.Item + enchantString
}

type ItemMetadata struct {
	LocalizedNames struct {
		ENUS string `json:"EN-US"`
		DEDE string `json:"DE-DE"`
		FRFR string `json:"FR-FR"`
		RURU string `json:"RU-RU"`
		PLPL string `json:"PL-PL"`
		ESES string `json:"ES-ES"`
		PTBR string `json:"PT-BR"`
		ZHCN string `json:"ZH-CN"`
		KOKR string `json:"KO-KR"`
	} `json:"LocalizedNames"`
	UniqueName string `json:"UniqueName"`
}

type Stream struct {
	AlbionName   string        `json:"albion_name"`
	StreamUrl    string        `json:"stream_url"`
	TiwtchName   string        `json:"twitch_name"`
	Title        string        `json:"title"`
	ViewerCount  int           `json:"viewer_count"`
	StartedAt    time.Time     `json:"started_at"`
	Language     string        `json:"language"`
	ThumbnailURL string        `json:"thumbnail_url"`
	Events       []StreamEvent `json:"events"`
}

type StreamEvent struct {
	Time   int    `json:"time"`
	Id     int    `json:"id"`
	Weapon string `json:"weapon"`
	IsKill bool   `json:"is_kill"`
}
