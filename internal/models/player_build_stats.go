package models

type PlayerBuildStats struct {
	PlayerName string      `json:"player_name"`
	Builds     []BuildStat `json:"builds"`
}

type BuildStats struct {
	MetricName string      `json:"metric_name"`
	Builds     []BuildStat `json:"builds"`
}

type BuildStat struct {
	Build            Build    `json:"build"`
	Usages           int      `json:"usages"`
	AverageItemPower float64  `json:"average_item_power"`
	KillFame         int      `json:"kill_fame"`
	DeathFame        int      `json:"death_fame"`
	Kills            int      `json:"kills"`
	Deaths           int      `json:"deaths"`
	Assists          int      `json:"assists"`
	FameRatio        *float64 `json:"fame_ratio"`
	WinRate          float64  `json:"win_rate"`
}

type Build struct {
	MainHand BuildItem `json:"main_hand"`
	OffHand  BuildItem `json:"off_hand"`
	Head     BuildItem `json:"head"`
	Body     BuildItem `json:"body"`
	Shoe     BuildItem `json:"shoe"`
	Cape     BuildItem `json:"cape"`
}

type BuildItem struct {
	Item string `json:"type"`
	Name string `json:"en_name"`
}
