create table build_stats (
	stat_name varchar(35),
	main_hand_item varchar(40),
	off_hand_item varchar(40),
	head_item varchar(40),
	body_item varchar(40),
	shoe_item varchar(40),
	cape_item varchar(40),
	time_added timestamp,
	usages int unsigned not null,
	avg_item_power smallint unsigned not null,
	kill_fame int unsigned not null,
	death_fame  int unsigned not null,
	kills int unsigned not null,
	deaths int unsigned not null,
	fame_ratio decimal(10, 2) unsigned,
	win_rate decimal (3,2) unsigned,
	PRIMARY KEY (stat_name, main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item)
);
select * from build_stats;
select * from build_stats where stat_name = '1v1_high_ip_7_day' order by win_rate desc;

-- 1v1 Weapon Stats
select
	main_hand_item,
	count(*) as usages,
	avg(item_power) as avg_item_power,
	sum(kill_fame) as kill_fame,
	sum(death_fame) as death_fame,
	sum(is_kill) as kills,
	sum(is_death) as deaths,
	sum(kill_fame) / sum(death_fame) as fame_ratio,
	sum(is_kill) / count(*) as win_rate
from 
(
	select
		kl.main_hand_item, k.item_power, e.total_kill_fame as kill_fame, 0 as death_fame, 1 as is_kill, 0 as is_death
	from killers as k
	join events as e on e.event_id = k.event_id 
	join victims as v on e.event_id = v.event_id
	join loadouts as kl on kl.id = k.loadout 
	WHERE e.participant_count = 1
	  AND k.is_primary = 1
	  AND e.party_size = 1
	  and k.item_power > 1200 and v.item_power > 1200 -- Slayer
	  AND e.time > DATE_SUB(NOW(), INTERVAL 7 DAY)
	UNION ALL
	select
		vl.main_hand_item, v.item_power, 0 as kill_fame, e.total_kill_fame as death_fame, 0 as is_kill, 1 as is_death
	from victims as v
	join events as e on e.event_id = v.event_id 
	join killers as k on k.event_id = e.event_id and k.is_primary = 1
	join loadouts as vl on vl.id = v.loadout 
	WHERE e.participant_count = 1
	  AND e.party_size = 1
	  AND v.item_power > 1200 and k.item_power > 1200 -- Slayer
	  AND e.time > DATE_SUB(NOW(), INTERVAL 7 DAY)
) as t
group by main_hand_item
having usages > 300;

-- 1v1 Build Stats
replace into build_stats
select
	'1v1_high_ip_7_day' as stat_name, main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item,
	now() as time_added,
	count(*) as usages,
	avg(item_power) as avg_item_power,
	sum(kill_fame) as kill_fame,
	sum(death_fame) as death_fame,
	sum(is_kill) as kills,
	sum(is_death) as deaths,
	sum(kill_fame) / sum(death_fame) as fame_ratio,
	sum(is_kill) / count(*) as win_rate
from 
(
	select
		main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item, k.item_power, e.total_kill_fame as kill_fame, 0 as death_fame, 1 as is_kill, 0 as is_death
	from killers as k
	join events as e on e.event_id = k.event_id
	join loadouts as kl on kl.id = k.loadout
	join victims as v on e.event_id = v.event_id
	WHERE e.participant_count = 1
	  AND k.is_primary = 1
	  AND e.party_size = 1
	  AND kl.main_hand_item != ""
	  and k.item_power > 1200 and v.item_power > 1200 -- Slayer
	  AND e.time > DATE_SUB(NOW(), INTERVAL 7 DAY)
	UNION ALL
	select
		main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item, v.item_power, 0 as kill_fame, e.total_kill_fame as death_fame, 0 as is_kill, 1 as is_death
	from victims as v
	join events as e on e.event_id = v.event_id 
	join loadouts as vl on vl.id = v.loadout 
	join killers as k on k.event_id = e.event_id and k.is_primary = 1
	WHERE e.participant_count = 1
	  AND e.party_size = 1
	  AND vl.main_hand_item != ""
	  AND v.item_power > 1200 and k.item_power > 1200 -- Slayer
	  AND e.time > DATE_SUB(NOW(), INTERVAL 7 DAY)
) as t
group by main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item
having usages > 50;

-- 2v2 Weapon Stats
replace into build_stats
select
	'2v2_high_ip_7_day' as stat_name, main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item,
	count(*) as usages,
	avg(item_power) as avg_item_power,
	sum(kill_fame) as kill_fame,
	sum(death_fame) as death_fame,
	sum(is_kill) as kills,
	sum(is_death) as deaths,
	sum(kill_fame) / sum(death_fame) as fame_ratio,
	sum(is_kill) / count(*) as win_rate
from 
(
	select
		main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item, k.item_power, e.total_kill_fame as kill_fame, 0 as death_fame, 1 as is_kill, 0 as is_death
	from killers as k
	join events as e on e.event_id = k.event_id
	join loadouts as kl on kl.id = k.loadout
	join victims as v on e.event_id = v.event_id
	WHERE e.participant_count = 2
	  AND e.party_size = 2
	  AND kl.main_hand_item != ""
	  and k.item_power > 1100 and v.item_power > 1100
	  AND e.time > DATE_SUB(NOW(), INTERVAL 7 DAY)
	UNION ALL
	select
		main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item, v.item_power, 0 as kill_fame, e.total_kill_fame as death_fame, 0 as is_kill, 1 as is_death
	from victims as v
	join events as e on e.event_id = v.event_id 
	join loadouts as vl on vl.id = v.loadout 
	join killers as k on k.event_id = e.event_id and k.is_primary = 1
	WHERE e.participant_count = 2
	  AND e.party_size = 2
	  AND vl.main_hand_item != ""
	  AND v.item_power > 1100 and k.item_power > 1100
	  AND e.time > DATE_SUB(NOW(), INTERVAL 7 DAY)
) as t
group by main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item
having usages > 50;



-- Top Players For Weapon (1v1)

-- Top Players 1v1



-- Player Favorite Weapons (Last 500 kills)
select
	main_hand_item,
	count(*) as usages,
	avg(item_power) as avg_item_power,
	sum(kill_fame) as kill_fame,
	sum(death_fame) as death_fame,
	sum(is_kill) as kills,
	sum(is_death) as deaths,
	sum(is_assist) as assists,
	sum(kill_fame) / sum(death_fame) as fame_ratio,
	(sum(is_kill) + sum(is_assist)) / sum(deaths) as kdr
from 
(
	select
		time, kl.main_hand_item, k.item_power, e.total_kill_fame as kill_fame, 0 as death_fame, is_primary as is_kill, 0 as is_death, not is_primary as is_assist
	from killers as k
	join events as e on e.event_id = k.event_id 
	join loadouts as kl on kl.id = k.loadout 
	WHERE k.name = :name
	UNION ALL
	select
		time, vl.main_hand_item, v.item_power, 0 as kill_fame, e.total_kill_fame as death_fame, 0 as is_kill, 1 as is_death, 0 as is_assist
	from victims as v
	join events as e on e.event_id = v.event_id 
	join loadouts as vl on vl.id = v.loadout 
	WHERE v.name = :name
	order by time desc limit 500
) as t
group by main_hand_item
order by usages desc limit 10;

-- Player Favorite Builds (Last 500 kills)
-- possible filters:
	-- group size: 1v1, 2v2, 5v5, ZvZ
select
	main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item,
	count(*) as usages,
	avg(item_power) as avg_item_power,
	sum(kill_fame) as kill_fame,
	sum(death_fame) as death_fame,
	sum(is_kill) as kills,
	sum(is_death) as deaths,
	sum(is_assist) as assists,
	sum(kill_fame) / sum(death_fame) as fame_ratio,
	(sum(is_kill) + sum(is_assist)) / sum(deaths) as kdr
from 
(
	select
		time, kl.main_hand_item, kl.off_hand_item, kl.head_item, kl.body_item, kl.shoe_item, kl.cape_item, k.item_power, e.total_kill_fame as kill_fame, 0 as death_fame, is_primary as is_kill, 0 as is_death, not is_primary as is_assist
	from killers as k
	join events as e on e.event_id = k.event_id 
	join loadouts as kl on kl.id = k.loadout 
	WHERE k.name = :name
	UNION ALL
	select
		time, vl.main_hand_item, vl.off_hand_item, vl.head_item, vl.body_item, vl.shoe_item, vl.cape_item, v.item_power, 0 as kill_fame, e.total_kill_fame as death_fame, 0 as is_kill, 1 as is_death, 0 as is_assist
	from victims as v
	join events as e on e.event_id = v.event_id 
	join loadouts as vl on vl.id = v.loadout
	WHERE v.name = :name
	order by time desc limit 500
) as t
group by main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item
order by usages desc limit 20;

-- Player Top Builds 2v2 (by time range)

-- Player Top Builds ZvZ (by time range)